<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Resources as ResourcesControllers;

/*
|--------------------------------------------------------------------------
| API [Resources]
|--------------------------------------------------------------------------
*/

Route::get('areas/{id_sucursal}', [ResourcesControllers\AreasController::class, 'index'])->name('areas');
Route::get('tcs-categorias/{id_sucursal}', [ResourcesControllers\TCSCategoriasController::class, 'index'])->name('tcs_categorias');
Route::get('tcs-subcategorias/{id_sucursal}/{id_categoria}', [ResourcesControllers\TCSSubcategoriasController::class, 'index'])->name('tcs_subcategorias');
