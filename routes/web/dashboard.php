<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Dashboard as Dashboard;
use App\Http\Controllers\Web\Resources as Resources;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function() {
    return redirect()->route('web.dashboard.inicio');
});

Route::get('inicio', [Dashboard\DashboardController::class, 'inicio'])->name('inicio');
Route::resource('tickets', Resources\TicketsController::class)->names('tickets');
