<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Scopes\TicketsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tickets extends Model
{
    use HasFactory;
    /**
     * Conexión a utilizar
     *
     * @var string
     */
    protected $connection = 'tickets';

    /**
     * Nombre de la tabla a utilizar.
     *
     * @var string
     */
    protected $table = 'tickets';

    /**
     * Llave primaria a utilizar.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Los atributos que son asignables en masa.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    /**
     * Attributes
     */
    protected $appends = [
        //
    ];

    /**
     * Carpeta Evidencia Inicial
     *
     * @return String|null $carpeta_evidencia_inicial
     */
    public function getCarpetaEvidenciaAttribute()
    {
        $tb_sucursal = TBSucursal::query()
            ->where('sucursal', $this->sucursal)
            ->first();

        if (!$tb_sucursal) {
            return null;
        }

        if (!$tb_sucursal->tb_app) {
            return null;
        }

        return $tb_sucursal->tb_app->carpeta;
    }

    /**
     * URL Evidencia Inicial
     *
     * @return String|null $url_evidencia
     */
    public function getUrlEvidenciaInicialAttribute()
    {
        if ($this->carpeta_evidencia == null) {
            return null;
        }

        $nombre_imagen = $this->evidenciaInicial;

        if ($this->ticket_sumapp != null) {
            $base = "https://fotos.sumapp.cloud/Sumapp";
        } else {
            $base = "https://fotostickets.sumapp.cloud";
        }

        return "{$base}/{$this->carpeta_evidencia}/{$nombre_imagen}";
    }

    /**
     * URL Evidencia Inicial
     *
     * @return String|null $url_evidencia
     */
    public function getUrlEvidenciaFinalAttribute()
    {
        if ($this->carpeta_evidencia == null) {
            return null;
        }

        $nombre_imagen = $this->evidenciaFinal;
        $base = "https://fotostickets.sumapp.cloud";

        return "{$base}/{$this->carpeta_evidencia}/{$nombre_imagen}";
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('by_role', function (Builder $builder) {
            if (auth()->check()) {
                $auth = auth()->user();
                if (Str::contains($auth->rol_tickets, 'Corporativo')) {
                    $builder->where('empresa', $auth->empresa->c_nombre_empresa);
                } else {
                    $builder->where('sucursal', $auth->sucursal->sucursal);
                }
            }
        });
    }
}
