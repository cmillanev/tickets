<?php

namespace App\Http\Controllers\API\Resources;

use App\Models\TBSucursal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreasController extends Controller
{
    /**
     * Trae un catalogo de categorias basado en el id sucursal
     *
     * @param Int $id_sucursal
     *
     * @return TCSCategorias $collection
     */
    public function index(Int $id_sucursal)
    {
        $tb_sucursal = TBSucursal::query()
            ->where('id_sucursal', $id_sucursal)
            ->with(['areas'])
            ->firstOrFail();

        $areas = $tb_sucursal->areas->map(function ($item) {
            return [
                'id' => $item->id,
                'area' => $item->area_descripcion,
            ];
        });

        return response()->json($areas);
    }
}
