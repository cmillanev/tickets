<?php

namespace App\Http\Controllers\Web\Dashboard;

use App\Helpers\SweetAlert2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tickets;

class DashboardController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        //
    }

    /**
     * Retorna la vista de inicio
     *
     * @return View dashboard.inicio
     */
    public function inicio()
    {
        $tickets = Tickets::get();
        $status = (object) [
            'abiertos'    => $tickets->where('estatus', 'Abierto')->count(),
            'en_proceso'  => $tickets->where('estatus', 'En proceso')->count(),
            'cerrados'    => $tickets->where('estatus', 'Cerrado')->count(),
            'preventivos' => $tickets->where('accion', 'Preventivo')->count(),
            'correctivos' => $tickets->where('accion', 'Correctivo')->count()
        ];

        return view('dashboard.inicio', compact('status'));
    }
}
