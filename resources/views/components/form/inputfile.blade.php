<div>
    <label for="{{ $model }}" class="mb-2">{{ $label.':' }} {!! isset($required) ? '<span class="text-danger">*</span>' : '' !!}</label>
    <input
        type="file"
        class="form-control-file @error($model) is-invalid @enderror"
        id="{{ $model }}"
        name="{{ $model }}"
        {!! isset($required) ? 'required="required"' : '' !!}
        {!! isset($disabled) ? 'disabled="disabled"' : '' !!}
        {!! isset($accept) ? "accept=\"$accept\"" : '' !!}
        {!! isset($capture) ? "capture=\"$capture\"" : '' !!}
    >
    @error($model)<span class="font-italic text-danger">{{ $message }}</span>@enderror
</div>
