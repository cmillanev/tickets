@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Abrir Ticket</h1>
@stop

@section('content')
    <div class="pb-2">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('web.dashboard.tickets.store') }}" method="POST" enctype="multipart/form-data"
                    onsubmit="Loader.show()">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Sucursal" model="sucursal" onchange="getAreas(this.value)" required>
                                <option value="">Elige una sucursal</option>
                                @foreach ($sucursales as $sucursal)
                                    <option value="{{ $sucursal->id_sucursal }}"
                                        {{ old('sucursal') != $sucursal->id_sucursal ?: 'selected' }}>
                                        {{ $sucursal->sucursal }}
                                    </option>
                                @endforeach
                            </x-form.select>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Area" model="area" onchange="getCategorias()" required>
                                <option value="">Elige una area</option>
                            </x-form.select>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Categoría" model="categoria" onchange="getSubcategorias(this.value)"
                                required>
                                <option value="">Elige una categoría</option>
                            </x-form.select>
                        </div>

                         @switch(auth()->user()->id_empresa)
                             @case(27)

                            <div class="col-12 col-md-6 mb-3 ui-widget">
                                 <x-form.input label="Equipo" model="inventario" id="inventario" placeholder="Escribe" />

                            </div>
                            @break

                             @default
                            <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Subcategoría" model="subcategoria">
                                <option value="">Elige una subcategoría</option>
                            </x-form.select>
                            </div>
                         @endswitch
                            @switch(auth()->user()->id_empresa)
                             @case(27)
                            <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Tipo ticket" model="tipo_ticket" required>
                                <option value="">Elige tipo de ticket</option>
                                <option value="Preventivo">Preventivo</option>
                                <option value="Correctivo">Correctivo</option>
                                <option value="Modificaciones">Modificaciones</option>
                            </x-form.select>
                            </div>
                            @break

                             @default
                         <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Cuarto" model="cuarto" placeholder="Ingresa el cuarto" />
                        </div>
                         @endswitch

                            <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Prioridad" model="prioridad" required>
                                <option value="">Elige tipo de prioridad</option>
                                <option value="Alta">Alta</option>
                                <option value="Media">Media</option>
                                <option value="Baja">Baja</option>
                            </x-form.select>
                            </div>

                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Acción a realizar" model="accion_a_realizar"
                                placeholder="Ingresa la acción a realizar" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.textarea label="Descripción" model="descripcion"
                                placeholder="Ingresa una descripción" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.textarea label="Observaciones" model="observaciones"
                                placeholder="Ingresa algunas observaciones" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input type="number" label="Costo estimado" model="costo_estimado"
                                placeholder="Ingresa el costo estimado" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input type="date" label="Fecha estimada" model="fecha_estimada" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.inputfile capture="user" label="Evidencia de falla" model="evidencia" accept=".jpg,.jpeg,.png" />
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="text-right">
                                <a href="{{ route('web.dashboard.tickets.index') }}" class="btn btn-outline-danger">
                                    <i class="fas fa-times"></i> Cancelar
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-check"></i> Listo
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('css')
 <link
      href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"
      rel="stylesheet"
    />
@stop

@section('js')
  <script
      src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
      integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    ></script>
    <script>
        let id_sucursal_var;

        /**
         * Obtiene las areas y las coloca en el input area
         **/
        let areas = [];

        function getAreas(id_sucursal) {
            id_sucursal_var = id_sucursal;
            $('#area').html(new Option('Elige una area', ''));

            Loader.show();
            axios.get(`{{ url('api/resources/areas') }}/${id_sucursal_var}`)
                .then(async (response) => {
                    $.each(areas = await response.data, (index, item) => {
                        $('#area').append(new Option(item.area, item.id));
                    });
                }).catch((error) => {
                    areas = [];
                    console.log(`getAreas() - ${error}`);
                }).finally(() => {
                    Loader.hide();
                });
        }

        /**
         * Obtiene las categorias y las coloca en el input categoria
         **/
        let categorias = [];

        function getCategorias() {
            $('#categoria').html(new Option('Elige una categoría', ''));

            Loader.show();
            axios.get(`{{ url('api/resources/tcs-categorias') }}/${id_sucursal_var}`)
                .then(async (response) => {
                    $.each(categorias = await response.data, (index, item) => {
                        $('#categoria').append(new Option(item.categoria, item.id));
                    });
                }).catch((error) => {
                    categorias = [];
                    console.log(`getCategoria() - ${error}`);
                }).finally(async () => {
                    Loader.hide();
                });
        }

        /**
         * Obtiene las subcategorias y las coloca en el input subcategoria
         **/
        let subcategorias = [];

        function getSubcategorias(id_categoria) {
            $('#subcategoria').html(new Option('Elige una subcategoría', ''));

            Loader.show();
            axios.get(`{{ url('api/resources/tcs-subcategorias') }}/${id_sucursal_var}/${id_categoria}`)
                .then(async (response) => {
                    $.each(subcategorias = await response.data, (index, item) => {
                        $('#subcategoria').append(new Option(item.subcategoria, item.id));
                    });
                }).catch((error) => {
                    subcategorias = [];
                    console.log(`getSubcategorias() - ${error}`);
                }).finally(async () => {
                    Loader.hide();
                });
        }


    </script>

    <script>
        //Inventario
    $( function() {
     var availableTags = @JSON($inventario);
     $( "#inventario" ).autocomplete({
      source: availableTags
    });
    } );
    </script>
@stop
