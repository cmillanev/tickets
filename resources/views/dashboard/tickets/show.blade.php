@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Ver Ticket</h1>
@stop

@section('content')
    <div class="pb-2">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4 mb-3">
                        <x-form.input label="Folio" model="folio" :value="'#'.$ticket->id" disabled />
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <x-form.input label="Creación" model="creacion" :value="$ticket->created_at ?? 'Desconocido'" disabled />
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <x-form.input label="Actualización" model="actualizacion" :value="$ticket->updated_at ?? 'Ninguna'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Estado" model="estatus" :value="$ticket->estatus ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Prioridad" model="prioridad" :value="$ticket->prioridad ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Sucursal" model="sucursal" :value="$ticket->sucursal ?? 'No aplica'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Area" model="area" :value="$ticket->area ?? 'No aplica'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Categoria" model="categoria" :value="$ticket->categoria ?? 'No aplica'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Subcategoria" model="subcategoria" :value="$ticket->subcategoria ?? 'No aplica'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Cuarto" model="cuarto" :value="$ticket->habitacion ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Acción a realizar" model="accion_a_realizar" :value="$ticket->accion ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.textarea label="Descripción" model="descripcion" disabled>{{ $ticket->ticket_descripcion ?? '[No hay información]' }}</x-form.textarea>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.textarea label="Observaciones" model="observaciones" disabled>{{ $ticket->observaciones ?? '[No hay información]' }}</x-form.textarea>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Costo estimado" model="costo_estimado" :value="$ticket->costo_estimado ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Fecha estimada" model="fecha_estimada" :value="$ticket->fecha_estimada ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Costo real" model="costo_real" :value="$ticket->costo ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <x-form.input label="Fecha real" model="fecha_real" :value="$ticket->fecha ?? '[No hay información]'" disabled />
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <div>
                            <label class="mb-2">Evidencia de falla:</label>
                            @if ($ticket->evidenciaInicial != null)
                                <img class="img-fluid rounded" src="{{ $ticket->url_evidencia_inicial }}">
                            @else
                                <div class="font-italic">[No hay evidencia]</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <div>
                            <label class="mb-2">Evidencia de solución:</label>
                            @if ($ticket->evidenciaFinal != null)
                                <img class="img-fluid rounded" src="{{ $ticket->url_evidencia_final }}">
                            @else
                                <div class="font-italic">[No hay evidencia]</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <hr>
                        <div class="text-left">
                            <a class="btn btn-info" href="{{ route('web.dashboard.tickets.index') }}">
                                <i class="fas fa-angle-left"></i> Regresar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
