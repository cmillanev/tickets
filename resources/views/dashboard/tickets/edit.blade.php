@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Ticket</h1>
@stop

@section('content')
    <div class="pb-2">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('web.dashboard.tickets.update', ['ticket' => $ticket->id]) }}" method="POST"
                    enctype="multipart/form-data" onsubmit="Loader.show()">
                    @csrf @method('PUT')
                    <div class="row">
                        <div class="col-12 col-md-4 mb-3">
                            <x-form.input label="Folio" model="folio" :value="'#'.$ticket->id" readonly />
                        </div>
                        <div class="col-12 col-md-4 mb-3">
                            <x-form.input label="Creación" model="creacion" :value="$ticket->created_at ?? 'Desconocido'" readonly />
                        </div>
                        <div class="col-12 col-md-4 mb-3">
                            <x-form.input label="Actualización" model="actualizacion" :value="$ticket->updated_at ?? 'Ninguna'" readonly />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Estado" model="estatus" required>
                                <option value="">Elige un estado</option>
                                <option {{ (old('estatus') != 'Abierto' and $ticket->estatus != 'Abierto') ?: 'selected' }}>Abierto</option>
                                <option {{ (old('estatus') != 'En proceso' and $ticket->estatus != 'En proceso') ?: 'selected' }}>En proceso</option>
                                <option {{ (old('estatus') != 'Cerrado' and $ticket->estatus != 'Cerrado') ?: 'selected' }}>Cerrado</option>
                                @if(auth()->user()->rol_tickets == 'Administrador')
                                    <option {{ (old('estatus') != 'Cierre final' and $ticket->estatus != 'Cierre final') ?: 'selected' }}>Cierre final</option>
                                @endif
                            </x-form.select>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Prioridad" model="prioridad" required>
                                <option value="">Elige una prioridad</option>
                                <option {{ (old('prioridad') != 'Baja' and $ticket->prioridad != 'Baja') ?: 'selected' }}>Baja</option>
                                <option {{ (old('prioridad') != 'Media' and $ticket->prioridad != 'Media') ?: 'selected' }}>Media</option>
                                <option {{ (old('prioridad') != 'Alta' and $ticket->prioridad != 'Alta') ?: 'selected' }}>Alta</option>
                            </x-form.select>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Sucursal" model="sucursal" :value="$ticket->sucursal ?? 'No aplica'" readonly />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Area" model="area" :value="$ticket->area ?? 'No aplica'" readonly />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Categoria" model="categoria" :value="$ticket->categoria ?? 'No aplica'" readonly />
                        </div>
                              @switch(auth()->user()->id_empresa)
                             @case(27)

                            <div class="col-12 col-md-6 mb-3 ui-widget">
                                 <x-form.input label="Equipo" model="inventario" id="inventario" :value="$ticket->inventario" />

                            </div>
                            @break

                             @default
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Subcategoria" model="subcategoria" :value="$ticket->subcategoria ?? 'No aplica'" readonly />
                        </div>
                        @endswitch
                             @switch(auth()->user()->id_empresa)
                             @case(27)
                            <div class="col-12 col-md-6 mb-3">
                            <x-form.select label="Tipo ticket" model="tipo_ticket" required>
                                <option value="">Elige tipo de ticket</option>
                                <option {{ (old('tipo_ticket') != 'Preventivo' and $ticket->tipo_ticket != 'Preventivo') ?: 'selected' }}>Preventivo</option>
                                <option {{ (old('tipo_ticket') != 'Correctivo' and $ticket->tipo_ticket != 'Correctivo') ?: 'selected' }}>Correctivo</option>
                                <option {{ (old('tipo_ticket') != 'Modificaciones' and $ticket->tipo_ticket != 'Modificaciones') ?: 'selected' }}>Modificaciones</option>
                            </x-form.select>
                            </div>
                            @break

                             @default
                         <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Cuarto" model="cuarto" placeholder="Ingresa el cuarto" />
                        </div>
                         @endswitch
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input label="Acción a realizar" model="accion_a_realizar" placeholder="Ingresa la acción a realizar" :value="$ticket->accion"/>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.textarea label="Descripción" model="descripcion" placeholder="Ingresa una descripción">
                                {{ $ticket->ticket_descripcion }}</x-form.textarea>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.textarea label="Observaciones" model="observaciones"
                                placeholder="Ingresa algunas observaciones">{{ $ticket->observaciones }}</x-form.textarea>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input type="number" label="Costo estimado" model="costo_estimado" placeholder="Ingresa el costo estimado" :value="$ticket->costo_estimado" />
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <x-form.input type="date" label="Fecha estimada" model="fecha_estimada" :value="$ticket->fecha_estimada"/>
                        </div>
                        @if (auth()->user()->rol_tickets == 'Administrador')
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.input type="number" label="Costo real" model="costo_real" placeholder="Ingresa el costo real" :value="$ticket->costo" />
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.input type="date" label="Fecha real" model="fecha_real" :value="$ticket->fecha"/>
                            </div>
                        @endif
                        <div class="col-12 col-md-6 mb-3">
                            @if ($ticket->evidenciaInicial != null)
                            <div>
                                <label class="mb-2">Evidencia de falla:</label>
                                <img class="img-fluid rounded" src="{{ asset($ticket->evidenciaInicial) }}">
                            </div>
                            @else
                                <x-form.inputfile capture="user" label="Evidencia de falla" model="evidencia_inicial" accept=".jpg,.jpeg,.png" />
                            @endif
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            @if ($ticket->evidenciaFinal != null)
                            <div>
                                <label class="mb-2">Evidencia de solución:</label>
                                <img class="img-fluid rounded" src="{{ asset($ticket->evidenciaFinal) }}">
                            </div>
                            @else
                                <x-form.inputfile capture="user" label="Evidencia de solución" model="evidencia_final" accept=".jpg,.jpeg,.png" />
                            @endif
                        </div>
                        <div class="col-12">
                            <hr>
                            <div class="text-right">
                                <a href="{{ route('web.dashboard.tickets.index') }}" class="btn btn-outline-danger">
                                    <i class="fas fa-times"></i> Cancelar
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-check"></i> Listo
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
