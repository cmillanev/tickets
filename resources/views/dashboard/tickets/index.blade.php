@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Listado de Tickets</h1>
@stop

@section('content')
    <div class="pb-2">
        <div class="card">
            <div class="card-body">
                <x-datatable id="ticketsIndex" :disableSort="[7]">
                    <thead>
                        <tr>
                            <th>Folio</th>
                            <th>Estatus</th>
                            <th>Sucursal</th>
                            <th>Area</th>
                            <th>Categoría</th>
                            <th>Creación</th>
                            <th>Actualización</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tickets as $ticket)
                            <tr>
                                <td>{{ "#{$ticket->id}" }}</td>
                                <td>{{ $ticket->estatus }}</td>
                                <td>{{ $ticket->sucursal ?? 'No aplica' }}</td>
                                <td>{{ $ticket->area ?? 'No aplica' }}</td>
                                <td>{{ $ticket->categoria ?? 'No aplica' }}</td>
                                <td>{{ $ticket->created_at ?? 'Desconocido' }}</td>
                                <td>{{ $ticket->updated_at ?? 'Ninguna' }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm"
                                        href="{{ route('web.dashboard.tickets.show', ['ticket' => $ticket->id]) }}">
                                        <i class="far fa-eye"></i>
                                    </a>

                                    @if (auth()->user()->rol_tickers != 'Usuario')
                                        @if ($ticket->estatus != 'Cierre final')
                                            @if (!(auth()->user()->rol_tickets == 'Ejecutor' and $ticket->estatus == 'Cerrado'))
                                                <a class="btn btn-primary btn-sm"
                                                    href="{{ route('web.dashboard.tickets.edit', ['ticket' => $ticket->id]) }}">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            @endif
                                        @endif
                                    @endif

                                    @if (auth()->user()->rol_tickets == 'Administrador')
                                        <button type="button" onclick="eliminarItem('{{ $ticket->id }}')"
                                            class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                        <form id="formDeleteTicket{{ $ticket->id }}" class="hide"
                                            action="{{ route('web.dashboard.tickets.destroy', ['ticket' => $ticket->id]) }}"
                                            method="POST">
                                            @csrf @method('delete')
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </x-datatable>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        .hide {
            display: none;
        }
    </style>
@stop

@section('js')
    <script>
        function eliminarItem(idTicket) {
            Swal.fire({
                icon: 'question',
                title: '¿Deseas continuar?',
                text: `Estás a punto de eliminar el ticket #${idTicket}, ten en cuenta que esta acción es permanente`,
                confirmButtonText: 'Si, continuar',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
            }).then((res) => {
                if (res.isConfirmed) {
                    Loader.show();
                    $(`#formDeleteTicket${idTicket}`).submit();
                }
            });
        }
    </script>
@stop
