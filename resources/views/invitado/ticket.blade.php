@extends('layouts.clean')
@section('title', 'Invitado')

@section('content')
    <div class="m-4">
        <h4>Genera un ticket</h4>
        <hr>
        <div class="p-2">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('web.invitado.generar') }}" method="POST" enctype="multipart/form-data"
                        onsubmit="Loader.show()">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.select label="Sucursal" model="sucursal" readonly>
                                    <option value="{{ $session['sucursal']['id_sucursal'] }}" selected>{{ $session['sucursal']['sucursal'] }}</option>
                                </x-form.select>
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.select label="Area" model="area" onchange="getCategorias()" required>
                                    <option value="">Elige una area</option>
                                    @foreach ($areas as $item)
                                        <option value="{{ $item->id }}">{{ $item->area }}</option>
                                    @endforeach
                                </x-form.select>
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.select label="Categoría" model="categoria" onchange="getSubcategorias(this.value)"
                                    required>
                                    <option value="">Elige una categoría</option>
                                </x-form.select>
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.select label="Subcategoría" model="subcategoria">
                                    <option value="">Elige una subcategoría</option>
                                </x-form.select>
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.input label="Cuarto" model="cuarto" placeholder="Ingresa el cuarto" />
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.input label="Acción a realizar" model="accion_a_realizar"
                                    placeholder="Ingresa la acción a realizar" />
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.textarea label="Descripción" model="descripcion"
                                    placeholder="Ingresa una descripción" />
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.textarea label="Observaciones" model="observaciones"
                                    placeholder="Ingresa algunas observaciones" />
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <x-form.inputfile label="Evidencia de falla" capture="user" model="evidencia" accept=".jpg,.jpeg,.png" />
                            </div>
                            <div class="col-12">
                                <hr>
                                <div class="text-right">
                                    <a href="{{ route('web.auth.logout') }}" class="btn btn-outline-danger">
                                        <i class="fas fa-times"></i> Cancelar
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-check"></i> Listo
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        let id_sucursal_var = '{{ $session['sucursal']['id_sucursal'] }}';

        /**
         * Obtiene las categorias y las coloca en el input categoria
         **/
        let categorias = [];

        function getCategorias() {
            $('#categoria').html(new Option('Elige una categoría', ''));

            Loader.show();
            axios.get(`{{ url('api/resources/tcs-categorias') }}/${id_sucursal_var}`)
                .then(async (response) => {
                    $.each(categorias = await response.data, (index, item) => {
                        $('#categoria').append(new Option(item.categoria, item.id));
                    });
                }).catch((error) => {
                    categorias = [];
                    console.log(`getCategoria() - ${error}`);
                }).finally(async () => {
                    Loader.hide();
                });
        }

        /**
         * Obtiene las subcategorias y las coloca en el input subcategoria
         **/
        let subcategorias = [];

        function getSubcategorias(id_categoria) {
            $('#subcategoria').html(new Option('Elige una subcategoría', ''));

            Loader.show();
            axios.get(`{{ url('api/resources/tcs-subcategorias') }}/${id_sucursal_var}/${id_categoria}`)
                .then(async (response) => {
                    $.each(subcategorias = await response.data, (index, item) => {
                        $('#subcategoria').append(new Option(item.subcategoria, item.id));
                    });
                }).catch((error) => {
                    subcategorias = [];
                    console.log(`getSubcategorias() - ${error}`);
                }).finally(async () => {
                    Loader.hide();
                });
        }
    </script>
@endsection
